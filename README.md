# sparknlpdisplay

<!-- badges: start -->
[![Lifecycle: experimental](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://lifecycle.r-lib.org/articles/stages.html#experimental)
<!-- badges: end -->

This package is a port from Python of the [John Snow Labs spark-nlp-display](https://nlp.johnsnowlabs.com/docs/en/display).
It provides functions for nicely formatting displays of the NLP annotations created by Spark NLP.

## Installation

You can install the released version of sparknlpdisplay from Gitlab source with:

``` r
devtools::install_gitlab("dkincaid/sparknlpdisplay")
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(sparknlpdisplay)
## basic example code
```

